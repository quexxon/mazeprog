defmodule Maze do

  @doc ~S"""
  Convert a `grid` of cells into a simplified `Map` to facilitate
  rendering. The resulting `Map` maintains the same keys as the
  `grid`, but cells have the following simplified form indicating the
  conditional presence of walls:
 
  %{n: `bool`, e: `bool`, s: `bool`, w: `bool`}
  """
  def from_grid(grid) do
    for {coords, cell} <- grid.cells,
      into: %{},
      do: {coords, cell_walls(grid, cell)}
  end

  defp cell_walls(grid, cell) do
    {x, y} = cell.coords

    has_border? = fn (coords) ->
      if grid.cells[coords] do
	not MapSet.member?(cell.links, coords)
      else
	true
      end
    end

    n = has_border?.({x, y-1})
    e = has_border?.({x+1, y})
    s = has_border?.({x, y+1})
    w = has_border?.({x-1, y})

    %{n: n, e: e, s: s, w: w}
  end

  @doc ~S"""
  Render the given `maze` as a Unicode string.
  """
  def render(maze) do
    render(maze, {0, 0}, 0, "")
  end

  defp render(maze, {x, y} = p, 0, str) do
    n = if maze[p] do
      cond do
	p == {0, 0} -> # top-left corner
	  false
	x == 0 -> # left edge
	  true
	true ->
	  maze[{x-1, y-1}][:e] || false
      end
    else
      cond do
	y == 0 -> # top-right corner
          false
	maze[{x, y-1}] -> # bottom edge
	  maze[{x, y-1}][:w] || false
	true -> # right edge
	  true
      end
    end

    e = cond do
      maze[p] ->
	maze[p][:n] || false
      y == 0 -> # top-right corner
	false
      maze[{x, y-1}] -> # bottom edge
	maze[{x, y-1}][:s] || false
      true -> # right edge
	false
    end

    s = cond do
      maze[p] ->
	maze[p][:w] || false
      maze[{x-1, y}] -> # right edge
	true
      true -> # bottom edge
	false
    end 

    w = if maze[p] do
      cond do
	x == 0 -> # top-left corner
	  false
	y == 0 -> # top edge
	  true
	true ->
	  maze[{x-1, y-1}][:s] || false  
      end
    else
      cond do
	x == 0 -> # bottom-left corner
	  false
	maze[{x-1, y}] -> # right edge
	  maze[{x-1, y}][:n] || false
	true -> # bottom edge
	  true
      end
    end

    new_str = str <> get_char(n, e, s, w)

    cond do
      maze[p] ->
	render(maze, p, 1, new_str)
      maze[{x-1, y}] -> # right edge
	render(maze, {0, y+1}, 0, new_str <> "\n")
      maze[{x, y-1}] -> # bottom edge
	render(maze, p, 1, new_str)
      true -> # bottom-right corner
	new_str
    end
  end

  defp render(maze, {x, y} = p, 1, str) do
    new_str = cond do
      is_nil(maze[p]) -> # bottom edge
	str <> "─"
      maze[p][:n] ->
	str <> "─"
      true ->
	str <> " "
    end

    render(maze, {x+1, y}, 0, new_str)
  end

  defp get_char(n, e, s, w) do
    chars =
      %{{false, false, false, false} => " ",
	{true,  false, false, false} => "╵",
	{false, true,  false, false} => "╶",
	{false, false, true,  false} => "╷",
	{false, false, false, true } => "╴",
	{true,  true,  false, false} => "└",	      
	{true,  false, true,  false} => "│",
	{true,  false, false, true } => "┘",
	{false, true,  true,  false} => "┌",
	{false, true,  false, true } => "─",
	{false, false, true,  true } => "┐",
	{false, true,  true,  true } => "┬",
	{true,  false, true,  true } => "┤",
	{true,  true,  false, true } => "┴",	
	{true,  true,  true,  false} => "├",
	{true,  true,  true,  true } => "┼"}

    chars[{n, e, s, w}]
  end
end
