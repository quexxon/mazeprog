defmodule Cell do
  defstruct [:coords, :north, :east, :south, :west, :links]

  def link(grid, cell1, cell2, bidi \\ true) do
    cells = if bidi do
      grid.cells
      |> Map.put(cell1.coords, link(cell1, cell2))
      |> Map.put(cell2.coords, link(cell2, cell1))
    else
      grid.cells
      |> Map.put(cell1.coords, link(cell1, cell2))
    end

    %{grid | cells: cells}
  end

  defp link(cell1, cell2) do
    %{cell1 | links: MapSet.put(cell1.links, cell2.coords)}
  end
end
